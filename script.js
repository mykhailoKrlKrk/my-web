const addStudentButton = document.getElementById('addStudentButton');
const addStudentModal = document.getElementById('addStudentModal');
const cancelAddStudentButton = document.getElementById('cancelAddStudentButton');
const addStudentForm = document.getElementById('addStudentForm');
const studentTableBody = document.querySelector('#studentTable tbody');

let editedRowIndex = -1;
let selectedRow;

addStudentButton.addEventListener('click', function() {
    addStudentModal.classList.remove('hidden');
    clearErrorMessages(); 
});

cancelAddStudentButton.addEventListener('click', function() {
    addStudentModal.classList.add('hidden');
    clearErrorMessages(); 
});

function clearErrorMessages() {
  const errorElements = document.querySelectorAll('.error');
  errorElements.forEach(function(errorElement) {
      errorElement.classList.add('hidden');
  });
}

addStudentForm.addEventListener('submit', function(event) {
    event.preventDefault();

    document.querySelectorAll('.text-red-500').forEach(function(errorElement) {
        errorElement.classList.add('hidden');
    });

    const group = document.getElementById('group').value;
    const firstName = document.getElementById('firstName').value;
    const lastName = document.getElementById('lastName').value;
    const gender = document.getElementById('gender').value;
    const birthdate = document.getElementById('birthdate').value;

    let formIsValid = true;
    if (!group) {
        document.getElementById('groupError').classList.remove('hidden');
        formIsValid = false;
    }
    if (!firstName) {
        document.getElementById('firstNameError').classList.remove('hidden');
        formIsValid = false;
    }
    if (!lastName) {
        document.getElementById('lastNameError').classList.remove('hidden');
        formIsValid = false;
    }
    if (!gender) {
        document.getElementById('genderError').classList.remove('hidden');
        formIsValid = false;
    }
    if (!birthdate) {
        document.getElementById('birthdateError').classList.remove('hidden');
        formIsValid = false;
    }

    if (formIsValid) {
        const newRow = document.createElement('tr');
        newRow.innerHTML = `
            <td><input type="checkbox"></td>
            <td>${group}</td>
            <td>${firstName} ${lastName}</td>
            <td>${gender}</td>
            <td>${birthdate}</td>
            <td><span class="status active"></span></td>
            <td>
                <button class="editButton"><i class="fas fa-pencil-alt"></i></button>
                <button class="deleteButton"><i class="fas fa-times"></i></button>
            </td>
        `;
        newRow.querySelector('.editButton').addEventListener('click', function() {
          const row = this.closest('tr');
          const index = Array.from(row.parentNode.children).indexOf(row);
          editedRowIndex = index;
          showEditStudentModal(row);
      });
  
      newRow.querySelector('.deleteButton').addEventListener('click', function() {
          const row = this.closest('tr');
          selectedRow = row;
          const studentName = row.cells[2].textContent;
          showDeleteConfirmation(studentName);
      });

        studentTableBody.appendChild(newRow);

        addStudentForm.reset();

        addStudentModal.classList.add('hidden');
    }
});

document.querySelectorAll('.editButton').forEach(function(editButton, index) {
  editButton.addEventListener('click', function() {
      const row = this.closest('tr');
      editedRowIndex = index; 
      showEditStudentModal(row);
  });
});

function showEditStudentModal(row) {
  const cells = row.cells;
  const group = cells[1].textContent;
  const fullName = cells[2].textContent;
  const nameParts = fullName.split(' ');
  const firstName = nameParts[0];
  const lastName = nameParts[1];
  const gender = cells[3].textContent;
  const birthdate = cells[4].textContent;

  document.getElementById('editGroup').value = group;
  document.getElementById('editFirstName').value = firstName;
  document.getElementById('editLastName').value = lastName;
  document.getElementById('editGender').value = gender;
  document.getElementById('editBirthdate').value = birthdate;

  editStudentModal.classList.remove('hidden');
}

editStudentForm.addEventListener('submit', function(event) {
  event.preventDefault();

  const group = document.getElementById('editGroup').value;
  const firstName = document.getElementById('editFirstName').value;
  const lastName = document.getElementById('editLastName').value;
  const gender = document.getElementById('editGender').value;
  const birthdate = document.getElementById('editBirthdate').value;

  if (editedRowIndex !== -1) {

    const rows = document.querySelectorAll('tr');
    const selectedRow = rows[editedRowIndex + 1]; 
    const cells = selectedRow.cells;

    cells[1].textContent = group;
    cells[2].textContent = `${firstName} ${lastName}`;
    cells[3].textContent = gender;
    cells[4].textContent = birthdate;


    editStudentModal.classList.add('hidden');

    editedRowIndex = -1;
  } else {
    console.error('No row selected for editing.');
  }
});

document.getElementById('cancelEditStudentButton').addEventListener('click', function(event) {
  event.preventDefault();

  editStudentModal.classList.add('hidden');
});

function showDeleteConfirmation(studentName) {
  document.getElementById('deleteMessage').textContent = `Are you sure you want to delete ${studentName}?`;

  document.getElementById('deleteConfirmationModal').classList.remove('hidden');
}

document.querySelectorAll('.deleteButton').forEach(function(deleteButton) {
  deleteButton.addEventListener('click', function() {
    selectedRow = this.closest('tr');
    
    const studentName = selectedRow.cells[2].textContent;

    showDeleteConfirmation(studentName);
  });
});

document.getElementById('confirmDeleteButton').addEventListener('click', function() {

  selectedRow.remove();

  document.getElementById('deleteConfirmationModal').classList.add('hidden'); 
});

document.getElementById('cancelDeleteButton').addEventListener('click', function() {
  document.getElementById('deleteConfirmationModal').classList.add('hidden'); 
});




